# AutoDevOps
Nessa sessão iremos ver como funciona o AutoDevOps e seus principais conceitos.

## Lista de aulas:
1. AutoDevOps - Intro - P1
2. AutoDevOps - Intro - P2
3. AutoDevOps - Intro - P2
4. Criação do cluster
5. Configuração do DNS
6. Cluster-Management - APPS
  https://gitlab.com/auto-devops-ninja/cluster-management/-/blob/master/applications/cert-manager-1-4/helmfile.yaml
    Linha 64 - Trocar meu email: jonathanbaraldi@gmail.com - Pelo SEU!! o/o/o/
  https://gitlab.com/auto-devops-ninja/cluster-management/-/blob/master/applications/cert-manager-1-4/issuer-chart/values.yaml
    Linha 2 - Trocar meu email: jonathanbaraldi@gmail.com - Pelo SEU!! o/o/o/
    
7. Cloud DNS IP Nginx configuration
8. Gitlab Runner

9. App - Pipeline
10. App - Customização - DNS
11. App - Log e monitoramento
12. Operations Dashboard


Roadmap
Helm chart custom
Mais de 1 container 


# Cluster Management
Nessa sessão iremos entender e aplicar o cluster-managment, que é o repositório que controla as aplicações que iremos usar, como certificado, ingress, gitlab-runner, etc.

- Introdução
- como implementar - pipeline

- cert-manager
- ingress
- prometheus
- gitlab-runner


# Aplicação
Nessa sessão iremos ver como fazer o deployment de uma aplicação nodejs usando as melhores práticas e usando o pipeline automatizado do autodevops. Ao final iremos acessar a aplicação na url final, e monitorar as métricas e os logs pelo gitlab. 

## Auto DevOps features
Baseado nos estágios do DevOps, iremos usar o AutoDevOps para :]

- Build:
  - Auto Build
  - Auto Dependency Scanning
- Test:
  - Auto Test
  - Auto Browser Performance Testing
  - Auto Code Intelligence
  - Auto Code Quality
  - Auto Container Scanning
  - Auto License Compliance
- Deploy:
  - Auto Review Apps
  - Auto Deploy
- Monitor:
  - Auto Monitoring
- Secure:
  - Auto Dynamic Application Security Testing (DAST)
  - Auto Static Application Security Testing (SAST)
  - Auto Secret Detection

Para iniciarmos com o AutoDevOps:

Se você quer fazer o build, test e deploy do seu app:
1. Verificar os requisitos para o deployment.
2. Abilitar o Auto DevOps.
3. Seguir o  quick start guide, e ver o nosso treinamento. :)

Auto DevOps depende de muitos componententes, seja familiar com:
1. Integração contínua
2. Docker
3. GitLab Runner

Quando fizer o deployment para um cluster kubernetes, esteja certo que você está familiar com:
1. Kubernetes
2. Helm
3. Prometheus


# ==================================

1) Criação do cluster
2) Configuração do DNS - CloudDNS - Verificar o  wildcard.
3) Editar o helmfile e descomentar os 4 que queremos instalar.
	Acompanhar o pipeline

4) Pegar o IP publico do nginx-ingress e colocar no CloudDNS no wildcard.
5) Configurar o Gitab-Runner
6) Rodar pipeline da app 
	 ver toads etapas do pieline
	 arrumar o runner

```sh
Variable CODE_QUALITY_DISABLED enable
Variable CONTAINER_SCANNING_DISABLED enable
Variable DEPENDENCY_SCANNING_DISABLED true
Variable DOCKERFILE_PATH______ app1/Dockerfile
Variable LICENSE_MANAGEMENT_DISABLED true
Variable POSTGRES_ENABLED false
Variable SAST_DISABLED true
Variable TEST_DISABLED true
```

## DevOps Ninja Links

[Udemy]: https://www.udemy.com/course/devops-mao-na-massa-docker-kubernetes-rancher/
[youtube]: https://youtube.com/jonathanbaraldi
[github]: http://github.com/jonathanbaraldi
